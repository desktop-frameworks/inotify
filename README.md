# DFL Inotify

A thin wrapper around inotify.


### Dependencies:
* <tt>Qt Core (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/inotify dfl-inotify`
- Enter the `dfl-inotify` folder
  * `cd dfl-inotify`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release -Duse_qt_version=qt5`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know
